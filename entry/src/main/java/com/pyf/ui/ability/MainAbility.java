package com.pyf.ui.ability;


import com.pyf.common.ui.BaseAbility;
import com.pyf.ui.slice.MainAbilitySlice;

import ohos.aafwk.content.Intent;

public class MainAbility extends BaseAbility {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
    }
}
