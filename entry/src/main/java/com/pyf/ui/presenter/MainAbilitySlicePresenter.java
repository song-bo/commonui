package com.pyf.ui.presenter;

import com.pyf.common.bar.BottomBarComponentProvider;
import com.pyf.common.bar.FractionBarComponent;
import com.pyf.ui.ResourceTable;
import com.pyf.ui.bar.bottom.BottomBarInfo;
import com.pyf.ui.bar.bottom.BottomNavigationBar;
import com.pyf.ui.fraction.CategoryFraction;
import com.pyf.ui.fraction.FavoriteFraction;
import com.pyf.ui.fraction.FindFraction;
import com.pyf.ui.fraction.HomeFraction;
import com.pyf.ui.fraction.ProfileFraction;

import java.util.ArrayList;
import java.util.List;

import ohos.aafwk.ability.fraction.FractionManager;
import ohos.app.Context;

/**
 * 业务逻辑放到这个类来做
 *
 * @author 裴云飞
 * @date 2020/12/22
 */
public class MainAbilitySlicePresenter {

    private AbilitySliceProvider mAbilitySliceProvider;
    /**
     * 底部导航栏的数据
     */
    private List<BottomBarInfo<?>> bottomInfoList;
    /**
     * 底部导航栏
     */
    private BottomNavigationBar tabBottomLayout;

    private Context mContext;
    private final int defaultColor;
    private final int tintColor;
    private FractionBarComponent mFractionBarComponent;

    public MainAbilitySlicePresenter(AbilitySliceProvider abilitySliceProvider) {
        mAbilitySliceProvider = abilitySliceProvider;
        mContext = abilitySliceProvider.getContext();
        // 获取color.json文件中定义的颜色值
        defaultColor = mAbilitySliceProvider.getColor(ResourceTable.Color_default_color);
        tintColor = mAbilitySliceProvider.getColor(ResourceTable.Color_tint_color);
        initBottom();
    }

    private void initBottom() {
        tabBottomLayout = (BottomNavigationBar) mAbilitySliceProvider.findComponentById(ResourceTable.Id_bottom_navigation_bar);
        bottomInfoList = new ArrayList<>();
        // 获取string.json文件中定义的字符串
        String home = mAbilitySliceProvider.getString(ResourceTable.String_home);
        String favorite = mAbilitySliceProvider.getString(ResourceTable.String_favorite);
        String category = mAbilitySliceProvider.getString(ResourceTable.String_category);
        String find = mAbilitySliceProvider.getString(ResourceTable.String_find);
        String profile = mAbilitySliceProvider.getString(ResourceTable.String_profile);
        // 首页
        BottomBarInfo<Integer> homeInfo = new BottomBarInfo<>(home,
                ResourceTable.Media_home_normal,
                ResourceTable.Media_home_selected,
                defaultColor, tintColor);
        homeInfo.fraction = HomeFraction.class;
        // 收藏
        BottomBarInfo<Integer> favoriteInfo = new BottomBarInfo<>(favorite,
                ResourceTable.Media_favorite_normal,
                ResourceTable.Media_favorite_selected,
                defaultColor, tintColor);
        favoriteInfo.fraction = FavoriteFraction.class;
        // 分类
        BottomBarInfo<Integer> categoryInfo = new BottomBarInfo<>(category,
                ResourceTable.Media_category_normal,
                ResourceTable.Media_category_selected,
                defaultColor, tintColor);
        categoryInfo.fraction = CategoryFraction.class;
        // 发现
        BottomBarInfo<Integer> findInfo = new BottomBarInfo<>(find,
                ResourceTable.Media_recommend_normal,
                ResourceTable.Media_recommend_selected,
                defaultColor, tintColor);
        findInfo.fraction = FindFraction.class;
        // 我的
        BottomBarInfo<Integer> profileInfo = new BottomBarInfo<>(profile,
                ResourceTable.Media_profile_normal,
                ResourceTable.Media_profile_selected,
                defaultColor, tintColor);
        profileInfo.fraction = ProfileFraction.class;

        // 将每个条目的数据放入到集合
        bottomInfoList.add(homeInfo);
        bottomInfoList.add(favoriteInfo);
        bottomInfoList.add(categoryInfo);
        bottomInfoList.add(findInfo);
        bottomInfoList.add(profileInfo);
        // 设置底部导航栏的透明度
        tabBottomLayout.setBarBottomAlpha(0.85f);
        // 初始化所有的条目
        tabBottomLayout.initInfo(bottomInfoList);
        initFractionBarComponent();
        tabBottomLayout.addBarSelectedChangeListener((index, prevInfo, nextInfo) ->
                        // 显示fraction
                        mFractionBarComponent.setCurrentItem(index));
        // 设置默认选中的条目，该方法一定要在最后调用
        tabBottomLayout.defaultSelected(homeInfo);

        // 如果想让某个条目凸出来，可以按照下面的方式
        // 创建凸出来的条目，这张凸出来的图片肯定是要比不凸出来的图片大点
        //        BottomBarInfo<Integer> message = new BottomBarInfo<>(recommend,
        //                ResourceTable.Media_recommend_normal,
        //                ResourceTable.Media_recommend_normal);
        //        // 将条目添加集合
        //        infoList.add(message);
        //        // 在底部导航栏中查找该条目
        //        BottomBar bottomBar = tabBottomLayout.findBar(message);
        //        // 修改该条目的高度
        //        bottomBar.resetHeight(DisplayUtils.vp2px(mContext, 66));
    }

    private void initFractionBarComponent() {
        FractionManager fractionManage = mAbilitySliceProvider.getFractionManager();
        BottomBarComponentProvider provider = new BottomBarComponentProvider(bottomInfoList, fractionManage);
        mFractionBarComponent = (FractionBarComponent) mAbilitySliceProvider.findComponentById(ResourceTable.Id_fraction_bar_component);
        mFractionBarComponent.setProvider(provider);
    }
}
