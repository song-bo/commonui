package com.pyf.ui.fraction;

import com.pyf.common.ui.BaseFraction;
import com.pyf.ui.ResourceTable;

/**
 * @author 裴云飞
 * @date 2020/12/30
 */

public class FavoriteFraction extends BaseFraction {

    @Override
    public int getUIContent() {
        return ResourceTable.Layout_fraction_favorite;
    }

    @Override
    public void initComponent() {

    }
}
