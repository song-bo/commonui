package com.pyf.ui.fraction;

import com.pyf.common.ui.BaseFraction;
import com.pyf.ui.ResourceTable;
import com.pyf.ui.bar.common.IBarLayout;
import com.pyf.ui.bar.top.TopBarInfo;
import com.pyf.ui.bar.top.TopNavigationBar;
import com.pyf.ui.fraction.home.AntiviralFraction;
import com.pyf.ui.fraction.home.BeiJingFraction;
import com.pyf.ui.fraction.home.FollowFraction;
import com.pyf.ui.fraction.home.HotFraction;
import com.pyf.ui.fraction.home.ImportantFraction;
import com.pyf.ui.fraction.home.MusicFraction;
import com.pyf.ui.fraction.home.NovelFraction;
import com.pyf.ui.fraction.home.QuestionFraction;
import com.pyf.ui.fraction.home.RecommendFraction;
import com.pyf.ui.fraction.home.SportFraction;
import com.pyf.ui.fraction.home.TimesFraction;
import com.pyf.ui.fraction.home.VideoFraction;
import com.pyf.ui.refresh.IRefresh;
import com.pyf.ui.refresh.SwipeRefreshLayout;
import com.pyf.ui.refresh.TextOverComponent;
import com.pyf.ui.toast.Toast;

import java.util.ArrayList;
import java.util.List;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

/**
 * @author 裴云飞
 * @date 2020/12/30
 */

public class HomeFraction extends BaseFraction {

    /**
     * 顶部导航栏的数据
     */
    private List<TopBarInfo<?>> topInfoList;

    private List<Class<? extends Fraction> > mFractions = new ArrayList<>();

    /**
     * 顶部导航栏
     */
    private TopNavigationBar topNavigationBar;
    private int defaultColor;
    private int tintColor;
    private String[] bars = new String[]{
            "关注",
            "推荐",
            "热榜",
            "抗疫",
            "视频",
            "小说",
            "时代",
            "北京",
            "问答",
            "要闻",
            "体育",
            "音乐"
    };

    @Override
    public int getUIContent() {
        return ResourceTable.Layout_fraction_home;
    }

    @Override
    public void initComponent() {
        initTop();
        initRefresh();
    }

    private void initTop() {
        defaultColor = getColor(ResourceTable.Color_default_color);
        tintColor = getColor(ResourceTable.Color_tint_color);
        mFractions.add(FollowFraction.class);
        mFractions.add(RecommendFraction.class);
        mFractions.add(HotFraction.class);
        mFractions.add(AntiviralFraction.class);
        mFractions.add(VideoFraction.class);
        mFractions.add(NovelFraction.class);
        mFractions.add(TimesFraction.class);
        mFractions.add(BeiJingFraction.class);
        mFractions.add(QuestionFraction.class);
        mFractions.add(ImportantFraction.class);
        mFractions.add(SportFraction.class);
        mFractions.add(MusicFraction.class);
        topNavigationBar = (TopNavigationBar) mComponent.findComponentById(ResourceTable.Id_top_navigation_bar);
        topInfoList = new ArrayList<>();
        // 初始化顶部导航栏
        for (int i = 0; i < bars.length; i++) {
            String bar = bars[i];
            TopBarInfo<Integer> info = new TopBarInfo<>(bar, defaultColor, tintColor);
            info.fraction = mFractions.get(i);
            topInfoList.add(info);
        }
        topNavigationBar.initInfo(topInfoList);
        topNavigationBar.addBarSelectedChangeListener(new IBarLayout.OnBarSelectedListener<TopBarInfo<?>>() {
            @Override
            public void onBarSelectedChange(int index, TopBarInfo<?> prevInfo, TopBarInfo<?> nextInfo) {
                Toast.show(getFractionAbility(), nextInfo.name);
            }
        });
        topNavigationBar.defaultSelected(topInfoList.get(0));
    }

    private void initRefresh() {
        SwipeRefreshLayout refresh = (SwipeRefreshLayout) mComponent.findComponentById(ResourceTable.Id_refresh);
        // 创建下拉刷新头部组件
        TextOverComponent overView = new TextOverComponent(getFractionAbility());
        // 设置下拉刷新的头部组件
        refresh.setRefreshOverComponent(overView);
        refresh.setRefreshListener(new IRefresh.RefreshListener() {
            @Override
            public void onRefresh() {
                new EventHandler(EventRunner.getMainEventRunner()).postTask(new Runnable() {
                    @Override
                    public void run() {
                        // 正在刷新，在这里处理业务逻辑
                        refresh.refreshFinish();
                    }
                }, 1000);
            }

            /**
             * 允许下拉刷新
             *
             * @return
             */
            @Override
            public boolean enableRefresh() {
                return true;
            }
        });
    }
}
