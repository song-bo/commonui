package com.pyf.common.ui;


import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;

/**
 * @author 裴云飞
 * @date 2020/12/20
 */

public class BaseAbility extends FractionAbility {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
    }

}
