package com.pyf.common.bar;

import com.pyf.ui.bar.bottom.BottomBarInfo;

import java.util.List;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionManager;

/**
 * @author 裴云飞
 * @date 2020/12/30
 */
public class BottomBarComponentProvider extends BarComponentProvider {

    private List<BottomBarInfo<?>> mInfoList;

    public BottomBarComponentProvider(List<BottomBarInfo<?>> infoList, FractionManager fractionManager) {
        super(fractionManager);
        mInfoList = infoList;

    }

    @Override
    public Fraction getFraction(int position) {
        try {
            return mInfoList.get(position).fraction.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int getCount() {
        return mInfoList.size();
    }

}
