package com.poetry.navigationbar.bottom;

import com.poetry.navigationbar.common.TopBottomBarInfo;

import ohos.aafwk.ability.fraction.Fraction;

/**
 * 底部对话框中每个条目的数据，泛型对应的是文字的颜色，颜色可以是整型，也可以是字符串，具体是哪种类型的颜色，由调用者来决定
 *
 * @author 裴云飞
 * @date 2020/12/22
 */
public class BottomBarInfo<Color> extends TopBottomBarInfo {

    public enum BarType {
        /**
         * 显示图片和文案
         */
        IMAGE_TEXT,
        /**
         * 只显示图片
         */
        IMAGE,
        TEXT
    }

    /**
     * 条目的名称
     */
    public String name;
    public BarType tabType;
    public Class<? extends Fraction> fraction;

    public BottomBarInfo() {

    }

    public BottomBarInfo(String name, int defaultImage, int selectedImage) {
        this.name = name;
        this.defaultImage = defaultImage;
        this.selectedImage = selectedImage;
        this.tabType = BarType.IMAGE;
    }

    public BottomBarInfo(String name, int defaultImage, int selectedImage, Color defaultColor, Color tintColor) {
        this.name = name;
        this.defaultImage = defaultImage;
        this.selectedImage = selectedImage;
        this.defaultColor = defaultColor;
        this.tintColor = tintColor;
        this.tabType = BarType.IMAGE_TEXT;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BarType getTabType() {
        return tabType;
    }

    public void setTabType(BarType tabType) {
        this.tabType = tabType;
    }

    public Class<? extends Fraction> getFraction() {
        return fraction;
    }

    public void setFraction(Class<? extends Fraction> fraction) {
        this.fraction = fraction;
    }
}
