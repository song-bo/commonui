package com.poetry.navigationbar.common;

import com.poetry.navigationbar.top.TopBarInfo;

import ohos.aafwk.ability.fraction.Fraction;

/**
 * @author 裴云飞
 * @date 2020/12/30
 */

public class TopBottomBarInfo<Color> {

    /**
     * 默认的图片
     */
    public int defaultImage;
    /**
     * 选中后的图片
     */
    public int selectedImage;
    /**
     * 默认的文字颜色
     */
    public Color defaultColor;
    /**
     * 选中后的文字颜色
     */
    public Color tintColor;

    public String name;
    public TopBarInfo.BarType barType;
    public Class<? extends Fraction> fraction;
}
