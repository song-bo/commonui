package com.pyf.ui.bar.top;

import com.pyf.ui.bar.common.TopBottomBarInfo;

/**
 * 顶部导航栏中每个条目的数据，泛型对应的是文字的颜色，颜色可以是整型，
 * 也可以是字符串，具体是哪种类型的颜色，由调用者来决定
 *
 * @author 裴云飞
 * @date 2020/12/26
 */
public class TopBarInfo<Color> extends TopBottomBarInfo {

    public enum BarType {

        /**
         * 显示图片
         */
        IMAGE,
        /**
         * 显示文字
         */
        TEXT
    }

    public TopBarInfo(int defaultImage, int selectImage) {
        this.defaultImage = defaultImage;
        this.selectedImage = selectImage;
        barType = BarType.IMAGE;
    }

    public TopBarInfo(String name, Color defaultColor, Color tintColor) {
        this.name = name;
        this.defaultColor = defaultColor;
        this.tintColor = tintColor;
        this.barType = BarType.TEXT;
    }
}
